---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: VR Biofeedback for Eating Disorders
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Marine Capallera
proposé par étudiant: 
mots-clés: [Virtual Reality, Unity, eating disorders, biofeedback]
langue: [F,E]
confidentialité: non
suite: non
---


## Description/Contexte

Eating disorders come with major health burdens and around one half of patients do not make a full or lasting recovery after receiving treatment. Thus, developing new therapies, which focus on the underlying mechanisms of eating disorders, is crucial. In eating disorders, the perception of one's bodily signals (such as heartbeat, stomach activity or breathing) seems to be altered. The perception of stomach signals seems to play a particularly important role in the perception and regulation of eating-specific signals such as hunger, satiety and fullness. Training the perception and regulation of gastric signals is therefore a promising new approach in the treatment of eating disorders. 
The goal of this project is to develop a virtual reality application to visualize stomach activity to increase its perception. In a second step, participants will learn to modulate their stomach activity through biofeedback. 
This project is a collaboration with the department of psychology of the University of Fribourg. The project can be continued as a bachelor thesis.


## Objectifs

The project will consist of the following tasks:
1. Analysis of Virtual Reality technology (ex. Unity)
2. Analysis of the application domain (which are the body signals to be considered, how they are perceived, how they can be measured, etc.)
3. Design of the virtual reality application to manage and visualise user bodily signals in 3D
4. Development and test of a proof of concept
