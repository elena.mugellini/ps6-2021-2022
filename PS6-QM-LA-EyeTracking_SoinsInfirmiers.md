---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Outil intelligent utilisant l'eye-tracking pour les soins infirmiers
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Quentin Meteier
  - Leonardo Angelini
mots-clés: [eye-tracking, santé, machine learning]
langue: [F,E]
confidentialité: non
suite: non
---

```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth]{img/Pupil_Invisible.jpg}
\end{center}
```


## Description/Contexte

Dans la formation en sciences infirmières, les étudiantes sont exposées à plusieurs modalités d'enseignement, telles que des cours théoriques, des stages en milieux cliniques et de pratiques en laboratoire de simulation. En particulier, le but de la simulation est de reproduire une situation clinique en totalité ou en partie dans un contexte sécuritaire et encadré pour permettre aux apprenantes de mieux comprendre et gérer une situation similaire lorsqu'elle se produira réellement dans leur pratique.

Récemment, un projet d'innovation pédagogique a été lancé par l'institut HumanTech (HEIA-FR), en collaboration avec la Hauté école de santé Fribourg (HedS). Le but est de développer un système intelligent utilisant l'eye tracking afin d'aider les professeurs à mieux comprendre les mécanismes cognitifs des étudiant-e-s en simulation. En effet, le suivi oculaire (Eye Tracking) fournit une image claire et précise de l'endroit où se concentre l'attention visuelle d'une personne. Ces informations peuvent être utilisées pour démontrer les processus cognitifs liés à des tâches particulières et évaluer le
niveau de compréhension affiché par les étudiants au cours du processus d'apprentissage.

Le but de ce projet de semestre est de pouvoir reconnaître automatiquement tout type d'objet ainsi que le patient (ou certaines parties de son corps) dans la vidéo enregistrée par des lunettes d'eye tracking. Pour cela, les données récoltées par les lunettes Pupil Invisible seront utilisées et combinées à des algorithmes d'intelligence artificielle, permettant ainsi la reconnaissance d'objets. Ces lunettes fournissent la trace du regard de l'utilisateur (rond rouge) superposée sur une vidéo de la vue de l'utilisateur (enregistrée avec une caméra). Dans un deuxième temps, le but sera de détecter combien de temps l'utilisateur a fixé chaque objet. Le but est aussi de créer un outil facilement transposable à d'autres domaines, par exemple comme support en tenps réel pour des malvoyants.


## Objectifs

- Analyse des techniques de reconnaissance d'objets
- Prise en main de l'API Pupil Invisible
- Conception du système (architecture)
- Implémentation de la reconnaissance d'objets/parties du corps
- Logging des objets dans un fichier avec marqueur de temps
- Implémentation du temps de fixation sur chaque objet/partie du corps
- Tests du système en situation réelle

## Contraintes

- Utilisation des lunettes d'Eye Tracking Pupil Invisible (fournies)
