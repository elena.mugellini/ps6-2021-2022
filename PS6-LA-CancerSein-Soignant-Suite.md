---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Application pour accompagner les patientes affectées par le cancer du sein
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
proposé par étudiant: Joel Vogt
mots-clés: [auto-suivi, cancer, Ionic ]
langue: [F,E]
confidentialité: non
suite: oui
---

## Description/Contexte

Dans le cadre du projet de semestre 5, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosuivi du cancer du sein a été imaginée et conçue. Un premier prototype basique a été dévéloppé en Ionic. Le but de ce projet est de concevoir et implementer une application web qui permettra aux soignants d'accompagner et conseiller les patientes affectées par le cancer du sein. L'application sera testée avec les utilisateurs cible (les soignants).


## Objectifs

- Analyse des besoins en termes de suivi et conseil du patient
- Conception iterative de l'application pour les soignants
- Implementation de l'application
- Tests utilisateur

## Contraintes

- Continuation de l'app developpée en Ionic
