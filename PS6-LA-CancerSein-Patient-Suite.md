---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Application pour l'autosuivi du cancer du sein
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
proposé par étudiant: Lucie Schaffner
mots-clés: [auto-suivi, cancer, Ionic ]
langue: [F,E]
confidentialité: non
suite: oui
---

## Description/Contexte

Dans le cadre du projet de semestre 5, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosuivi du cancer du sein a été imaginée et conçue. Un premier prototype basique a été dévéloppé en Ionic. Le but de ce projet est d'implementer une première version complète de l'application pout les patient qui puisse être testée avec des utilisateurs cible.


## Objectifs

- Analyse des librairies pour la gestion du calendrier er pour la création de graphiques
- Analyse et amélioration de la User Experience 
- Conception et implementation de l'application
- Tests utilisateur

## Contraintes

- Continuation de l'app developpée en Ionic
