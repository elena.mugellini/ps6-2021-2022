---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Application pour la creation de questionnaire en cascade et d'un système de recommandation
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
proposé par étudiant: Andrea Petrucci
mots-clés: [auto-suivi, cancer, Ionic React ]
langue: [F,E]
confidentialité: non
suite: oui
---

## Description/Contexte

Dans le cadre du projet de semestre 5, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosuivi de l'endometriose a été imaginée et conçue. Un premier prototype basique a été dévéloppé en Ionic React. Le but de ce projet est d'implementer une inteface pour les soignants qui permettra de definir un questionnaire en cascade qui sera administré aux patientes. Sur la bases des réponses, de recommendations pourront être fournies aux patientes.


## Objectifs

- Analyse des besoin aux niveaux des types de questions
- Analyse de librairie existanes pour le developpement de questionnaires
- Analyse des moteurs de règles pour la gestion des suggestions
- Conception et implementation de l'application
- Déploiement
- Tests utilisateur

## Contraintes

- Continuation de l'app developpée en Ionic
