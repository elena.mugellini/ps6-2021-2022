---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Application pour l'autosuivi de l'endometriose
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
proposé par étudiant: Sam Corpataux
mots-clés: [auto-suivi, cancer, Ionic React ]
langue: [F,E]
confidentialité: non
suite: oui
---

## Description/Contexte

Dans le cadre du projet de semestre 5, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosuivi de l'endometriose a été imaginée et conçue. Un premier prototype basique a été dévéloppé en Ionic React. Le but de ce projet est d'implementer une première version complète de l'application pour les patientes, qui puisse être testée avec des utilisateurs cible.


## Objectifs

- Analyse des la création de graphiques et statistiques
- Analyse et amélioration de la User Experience 
- Conception et implementation de l'application
- Déploiement
- Tests utilisateur

## Contraintes

- Continuation de l'app developpée en Ionic
