---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: wakeword pour agent conversationnel
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
  - Mira El Kamali
proposé par étudiant: Hirschi Laurent
mots-clés: [teams, avatarbot]
langue: [F,E]
confidentialité: non
suite: oui
---

## Contexte

Un mot d'éveil est un signal pour que l'ordinateur commence à passer de l'écoute passive à l'écoute active. Il agit comme le "nom" de votre logiciel, signalant que l'utilisateur parle à l'application. "Hey Siri" et "Alexa" sont deux des mots d'éveil les plus connus.
Afin de rendre une interface plus personnalisée, l'une des méthodes consiste à personnaliser également votre wakeword.

Ce projet est une continuation du PS5
L'objectif de ce projet est d'analyser le model de Mycroft AI  et sinon, créer un moteur de wakeword qui permet aux utilisateurs de créer facilement des wakewords personnalisés. À la fin de ce projet, l'utilisateur devrait être en mesure de donner au système le nouveau mot d'éveil à utiliser. Le système devrait aussi détecter ce mot lorsque l'utilisateur l'appelle.

L'étudiant est chargé dans un premier temps d'analyser le modèle de Mycroft avec plusieurs échantillons ou d'essayer de rentrainer le modele sur un serveur ou dans le cloud. L'étudiant peut aussi en deuxième temps recréer son nouveau modèle de wakeword. 

L'étudiant devra à la fin faire des tests pour valider le fonctionnement de son système.

## Objectifs


- Analyse du modèle de mycroft AI
- Trouver une alternative
- Implémentation du prototype 
- Tests 

## Contraintes

- Python
